package com.weather.test_config;

import com.weather.controller.WeatherController;
import com.weather.service.WeatherService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * Created by : Olha_Bila
 * Created at : 03-02-2016
 */
@Configuration
public class TestRestServiceConfig {

    @Bean(name = "testWeatherService")
    public WeatherService weatherServiceMock() {
        return Mockito.mock(WeatherService.class);
    }

    @Bean(name = "testWeatherController")
    public WeatherController weatherControllerMock() { return Mockito.mock(WeatherController.class); }
}
