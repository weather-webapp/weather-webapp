package com.weather.test_config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/*
 * Created by : Olha_Bila
 * Created at : 03-02-2016
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {
        "com.weather"
})
public class TestRestServiceWebConfig extends WebMvcConfigurerAdapter {
}
