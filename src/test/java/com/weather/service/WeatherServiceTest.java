package com.weather.service;

import com.weather.dto.owm_dtos.MainDto;
import com.weather.dto.owm_dtos.OWMDto;
import com.weather.test_config.TestRestServiceConfig;
import com.weather.test_config.TestRestServiceWebConfig;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/*
 * Created by : Olha_Bila
 * Created at : 03-02-2016
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRestServiceConfig.class, TestRestServiceWebConfig.class })
@WebAppConfiguration
public class WeatherServiceTest {

    private static final String OWM_REQUEST_URL = "http://api.openweathermap.org/data/2.5/weather?zip=10001,us&appId=c27a015cd87215e1dbfeb5e8923e37ae";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    @Qualifier("testWeatherService")
    private WeatherService weatherServiceMock;

    @Before
    public void setup() {
        Mockito.reset(weatherServiceMock);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Ignore
    @Test
    public void getCurrentWeatherByZipAndCountryCodes() throws Exception {
        OWMDto expectedResponse = new OWMDto();
        expectedResponse.setName("Hoboken");
        MainDto mainDto = new MainDto();
        mainDto.setTemp(new BigDecimal("282.54"));
        expectedResponse.setMain(mainDto);
        expectedResponse.setCod(HttpStatus.OK.value());

        when(weatherServiceMock.getCurrentWeatherByZipAndCountryCodes("10001", "us")).thenReturn(expectedResponse);

        mockMvc.perform(get(OWM_REQUEST_URL))
                .andExpect(status().isOk());

        verify(weatherServiceMock, times(1)).getCurrentWeatherByZipAndCountryCodes("10001", "us");
        verifyNoMoreInteractions(weatherServiceMock);
    }

}