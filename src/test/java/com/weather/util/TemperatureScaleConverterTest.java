package com.weather.util;

import com.weather.dto.TemperatureScale;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/*
 * Created by : Olha_Bila
 * Created at : 03-02-2016
 */
public class TemperatureScaleConverterTest {

    private TemperatureScaleConverter converter;

    @Before
    public void setUp() throws Exception {
        converter = new TemperatureScaleConverter();
    }

    @Test
    public void testConvertWhenScaleIsC() throws Exception {
        BigDecimal expectedTemperature = new BigDecimal("6.053");
        BigDecimal actualTemperature = converter.convert(new BigDecimal("55"), TemperatureScale.C);
        assertNotNull(actualTemperature);
        assertEquals(expectedTemperature, actualTemperature);
    }

    @Test
    public void testConvertWhenScaleIsNullOrNotC() throws Exception {
        BigDecimal expectedTemperature = new BigDecimal("123");
        BigDecimal actualTemperature = converter.convert(new BigDecimal("123"), null);
        assertNotNull(actualTemperature);
        assertEquals(expectedTemperature, actualTemperature);
    }
}