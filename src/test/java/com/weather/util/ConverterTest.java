package com.weather.util;

import com.weather.dto.TemperatureScale;
import com.weather.dto.WeatherInfoDto;
import com.weather.dto.owm_dtos.MainDto;
import com.weather.dto.owm_dtos.OWMDto;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/*
 * Created by : Olha_Bila
 * Created at : 03-02-2016
 */
public class ConverterTest {

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    private Converter converter;

    @Before
    public void setUp() throws Exception {
        converter = new Converter();
        converter.setScaleConverter(new TemperatureScaleConverter());
    }

    @Test
    public void testToWeatherInfoDtoIfOWMResponseIsNull() throws Exception {
        thrown.expectMessage("There is no result according to your request. Please, try again.");
        converter.toWeatherInfoDto(null, TemperatureScale.C);
    }

    @Test
    public void testToWeatherInfoDtoIfOWMResponseCodeIsNotOk() throws Exception {
        thrown.expectMessage("OpenWeatherMap response status is not OK.");
        OWMDto owmResponse = new OWMDto();
        owmResponse.setCod(HttpStatus.BAD_REQUEST.value());
        converter.toWeatherInfoDto(owmResponse, TemperatureScale.C);
    }

    @Test
    public void testToWeatherInfoDtoIfSrcDtoMainIsNull() throws Exception {
        thrown.expectMessage("Unfortunately no temperature was specified for your city.");
        OWMDto owmResponse = new OWMDto();
        owmResponse.setCod(HttpStatus.OK.value());
        converter.toWeatherInfoDto(owmResponse, TemperatureScale.C);
    }

    @Test
    public void testToWeatherInfoDtoIfSrcDtoTemperatureIsNull() throws Exception {
        thrown.expectMessage("Unfortunately no temperature was specified for your city.");
        OWMDto owmResponse = new OWMDto();
        MainDto mainDto = new MainDto();
        owmResponse.setMain(mainDto);
        owmResponse.setMain(mainDto);
        owmResponse.setCod(HttpStatus.OK.value());
        converter.toWeatherInfoDto(owmResponse, TemperatureScale.C);
    }

    @Test
    public void testToWeatherInfoDtoSuccess() throws Exception {
        OWMDto owmResponse = new OWMDto();
        owmResponse.setName("Any name");
        MainDto mainDto = new MainDto();
        mainDto.setTemp(new BigDecimal("123"));
        owmResponse.setMain(mainDto);
        owmResponse.setCod(HttpStatus.OK.value());

        TemperatureScale expectedScale = TemperatureScale.C;

        WeatherInfoDto convertedResult = converter.toWeatherInfoDto(owmResponse, expectedScale);
        assertNotNull(convertedResult);
        assertEquals(owmResponse.getName(), convertedResult.getCityName());
        assertNotNull(convertedResult.getTemperature());
        assertEquals(expectedScale, convertedResult.getScale());
    }
}