package com.weather.dto;

/*
 * Created by : Olha_Bila
 * Created at : 01-02-2016
 */

import java.math.BigDecimal;

public class WeatherInfoDto {

    private String cityName;
    private TemperatureScale scale;
    private BigDecimal temperature;

    public WeatherInfoDto() {
    }

    public WeatherInfoDto(String cityName, TemperatureScale scale, BigDecimal temperature) {
        this.cityName = cityName;
        this.scale = scale;
        this.temperature = temperature;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public TemperatureScale getScale() {
        return scale;
    }

    public void setScale(TemperatureScale scale) {
        this.scale = scale;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "WeatherInfoDto{" +
                "cityName='" + cityName + '\'' +
                ", scale=" + scale +
                ", temperature=" + temperature +
                '}';
    }
}
