package com.weather.dto.owm_dtos;


import java.math.BigDecimal;

/*
 * Created by : Olha_Bila
 * Created at : 01-02-2016
 */
public class MainDto {

    private BigDecimal temp;

    public BigDecimal getTemp() {
        return temp;
    }

    public void setTemp(BigDecimal temp) {
        this.temp = temp;
    }

}
