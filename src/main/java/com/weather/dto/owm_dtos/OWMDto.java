package com.weather.dto.owm_dtos;

/*
 * Created by : Olha_Bila
 * Created at : 01-02-2016
 */

public class OWMDto {

    private MainDto main;
    private int id;
    private String name;
    private int cod;

    public MainDto getMain() {
        return main;
    }

    public void setMain(MainDto main) {
        this.main = main;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
}
