package com.weather.dto;

/*
 * Created by : Olha_Bila
 * Created at : 01-02-2016
 */
public enum TemperatureScale {

    C,
    F
}
