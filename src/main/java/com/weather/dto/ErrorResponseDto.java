package com.weather.dto;

/*
 * Created by : Olha_Bila
 * Created at : 02-02-2016
 */
public class ErrorResponseDto {

    private int code;
    private String message;

    public ErrorResponseDto(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "{\n" +
                "   \"code\": " + code + ",\n" +
                "   \"message\": \"" + message + "\"\n" +
                '}';
    }
}
