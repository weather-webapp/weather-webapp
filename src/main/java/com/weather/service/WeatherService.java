package com.weather.service;

import com.weather.dto.owm_dtos.OWMDto;

/*
 * Created by : Olha_Bila
 * Created at : 03-02-2016
 */
public interface WeatherService {

    OWMDto getCurrentWeatherByZipAndCountryCodes(String zipCode, String countryCode);
}
