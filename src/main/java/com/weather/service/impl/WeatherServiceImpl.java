package com.weather.service.impl;

/*
 * Created by : Olha_Bila
 * Created at : 01-02-2016
 */

import com.weather.dto.owm_dtos.OWMDto;
import com.weather.service.WeatherService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherServiceImpl implements WeatherService {

    private static final String URL_API = "http://api.openweathermap.org/data/2.5/";
    private static final String URL_CURRENT = "weather?";
    private static final String APP_ID = "c27a015cd87215e1dbfeb5e8923e37ae";

    private static final String PARAM_ZIP = "zip=";
    private static final String PARAM_APPID = "appId=";

    @Cacheable(value = "oneHourCache", key = "#zipCode + #countryCode")
    public OWMDto getCurrentWeatherByZipAndCountryCodes(String zipCode, String countryCode) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(getURL(zipCode, countryCode), OWMDto.class);
    }

    private String getURL(String zipCode, String countryCode) {
        return new StringBuilder()
                .append(URL_API).append(URL_CURRENT)
                .append(PARAM_ZIP).append(zipCode).append(",").append(countryCode).append("&")
                .append(PARAM_APPID).append(APP_ID)
                .toString();
    }

}

