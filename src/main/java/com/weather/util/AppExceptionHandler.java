package com.weather.util;

/*
 * Created by : Olha_Bila
 * Created at : 02-02-2016
 */

import com.weather.dto.ErrorResponseDto;
import com.weather.exception.HttpCodeException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler({HttpCodeException.class})
    public ResponseEntity<ErrorResponseDto> process(HttpCodeException exception) {
        return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto(exception.getStatus().value(),
                exception.getMessage()), exception.getStatus());
    }
}
