package com.weather.util;
/*
 * Created by : Olha_Bila
 * Created at : 01-02-2016
 */

import com.weather.dto.TemperatureScale;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class TemperatureScaleConverter {

    private static final int CALCULATION_PRECISION = 3;

    public BigDecimal convert(BigDecimal temperature, TemperatureScale scale) {
        BigDecimal result = temperature;

        if(TemperatureScale.C == scale) {

            result = ((temperature
                    .subtract(new BigDecimal(32)))
                    .multiply(new BigDecimal(5)))
                    .divide(new BigDecimal(19), CALCULATION_PRECISION, BigDecimal.ROUND_HALF_UP);
        }

        //the default scale is F so just return
        return result;
    }

}
