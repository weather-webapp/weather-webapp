package com.weather.util;

import com.weather.dto.ErrorResponseDto;
import org.springframework.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Created by : Olha_Bila
 * Created at : 02-02-2016
 */
public class AuthFilter implements Filter {

    private static final String USERNAME_HEADER = "username";
    private static final String PASSWORD_HEADER = "password";
    private static final String USERNAME = "user";
    private static final String PASSWORD = "123";

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        String username = ((HttpServletRequest) servletRequest).getHeader(USERNAME_HEADER);
        String password = ((HttpServletRequest) servletRequest).getHeader(PASSWORD_HEADER);
        if(username == null || password == null ||
                !(USERNAME.toUpperCase().equals(username.toUpperCase()) && PASSWORD.equals(password))) {
            ((HttpServletResponse)servletResponse).setStatus(HttpStatus.UNAUTHORIZED.value());
            servletResponse.setContentType("application/json");
            servletResponse.getOutputStream().println((new ErrorResponseDto(HttpStatus.UNAUTHORIZED.value(),
                    "Wrong credentials")).toString());
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public void destroy() {

    }
}
