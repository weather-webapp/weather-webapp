package com.weather.util;
/*
 * Created by : Olha_Bila
 * Created at : 01-02-2016
 */

import com.weather.dto.TemperatureScale;
import com.weather.dto.WeatherInfoDto;
import com.weather.dto.owm_dtos.OWMDto;
import com.weather.exception.HttpCodeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class Converter {

    @Autowired
    private TemperatureScaleConverter scaleConverter;

    public WeatherInfoDto toWeatherInfoDto(OWMDto srcDto, TemperatureScale scale) throws HttpCodeException {
        if(srcDto == null) {
            throw new HttpCodeException(HttpStatus.NOT_FOUND,
                    "There is no result according to your request. Please, try again.");
        }
        if(HttpStatus.OK.value() != srcDto.getCod()) {
            throw new HttpCodeException(HttpStatus.BAD_REQUEST,
                    "OpenWeatherMap response status is not OK.");
        }

        WeatherInfoDto targetDto = new WeatherInfoDto();
        targetDto.setCityName(srcDto.getName());
        if(srcDto.getMain() == null || srcDto.getMain().getTemp() == null) {
            throw new HttpCodeException(HttpStatus.NOT_FOUND,
                    "Unfortunately no temperature was specified for your city.");
        } else {
            targetDto.setTemperature(scaleConverter.convert(srcDto.getMain().getTemp(), scale));
        }
        targetDto.setScale(scale);

        return targetDto;
    }

    public void setScaleConverter(TemperatureScaleConverter scaleConverter) {
        this.scaleConverter = scaleConverter;
    }
}
