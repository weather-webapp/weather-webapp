package com.weather.controller;
/*
 * Created by : Olha_Bila
 * Created at : 31-01-2016
 */

import com.weather.dto.TemperatureScale;
import com.weather.dto.WeatherInfoDto;
import com.weather.dto.owm_dtos.OWMDto;
import com.weather.exception.HttpCodeException;
import com.weather.service.WeatherService;
import com.weather.util.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class WeatherController {

    @Autowired
    private WeatherService service;
    @Autowired
    private Converter converter;

    @RequestMapping(value = "/temp/current/{countryCode}/{zipCode}",
            produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public WeatherInfoDto getWeatherInfoByZipAndCountryCodes(@PathVariable String countryCode,
                                   @PathVariable String zipCode,
                                   @RequestParam(required = false) String scale) throws HttpCodeException {
        if(scale != null) {
            scale = scale.toUpperCase();
            if(!(TemperatureScale.C.name().equals(scale) || TemperatureScale.F.name().equals(scale))) {
                throw new HttpCodeException(HttpStatus.BAD_REQUEST,
                        "Request parameter 'scale' can be C(c) or F(f) only");
            }
        }

        OWMDto result = service.getCurrentWeatherByZipAndCountryCodes(zipCode, countryCode);
        return converter.toWeatherInfoDto(result, TemperatureScale.valueOf(scale));
    }
}
