package com.weather.exception;

import org.springframework.http.HttpStatus;

/*
 * Created by : Olha_Bila
 * Created at : 02-02-2016
 */
public class HttpCodeException extends RuntimeException {

    private HttpStatus status;

    public HttpCodeException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
